// Library Includes
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp> 
#include <string> 
#include <vector> 
#include <time.h>

#include "AssetManager.h"
#include "SpriteObject.h"
#include "Player.h"
#include "AnimatingObject.h"
#include "Wall.h"

int main()
{
	// Declare the SFML window, called gameWindow 
	sf::RenderWindow gameWindow;

	// Set up the SFML window and pass in the dimmensions and name
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Maze Escape", sf::Style::Titlebar | sf::Style::Close);

	// -----------------------------------------------
	// Game Setup
	// -----------------------------------------------
	
	// Player Code ------------------------------------------------------- 
	Player playerObject(sf::Vector2f(gameWindow.getSize().x / 2, gameWindow.getSize().y / 2));

	// Wall Code -------------------------------------------------------
	Wall wallObject(sf::Vector2f(200, 200));

	// Game Music -------------------------------------------------------
	// Declare a music variable, called gameMusic
	sf::Music gameMusic;

	// Open the music.ogg file and play it
	gameMusic.openFromFile("Assets/Audio/music.ogg");
	gameMusic.play();

	// Game Font -------------------------------------------------------
	// Declare a Font variable, called gameFont
	sf::Font gameFont;

	// Load the mainFont.ttf from the file
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");

	// Title Text
	// Declare a Text variable, called titletext
	sf::Text titleText;

	// Set titleText's font to gameFont
	titleText.setFont(gameFont);

	// Set the string that will display for titleText
	titleText.setString("Maze Escape");

	// Set the different parameters of titleText
	titleText.setCharacterSize(24);
	titleText.setFillColor(sf::Color::White);
	titleText.setStyle(sf::Text::Bold | sf::Text::Italic);
	titleText.setPosition(
		gameWindow.getSize().x / 2 - titleText.getLocalBounds().width / 2, 30);
	
	// Game Clock -------------------------------------------------------
	// Declare a Clock variable, called gameClock
	sf::Clock gameClock;

	while (gameWindow.isOpen())
	{
		// -----------------------------------------------
		// Input Section
		// -----------------------------------------------

		// Declare a variable to hold an Event, called gameEvent
		sf::Event gameEvent;

		// Loop through all events and poll them, putting each one into our gameEvent variable
		while (gameWindow.pollEvent(gameEvent))
		{
			// This will section repeat for each event waiting to be processed

			// Did the player try to close the window?
			if (gameEvent.type == sf::Event::Closed)
			{
				// If so, call the close function on the window.
				gameWindow.close();
			}

			// Player keybind input
			playerObject.Input();
		}

		// -----------------------------------------------
		// Update Section
		// -----------------------------------------------

		// Get the time passed since the last frame and restart the game clock
		sf::Time frameTime = gameClock.restart();

		// Move the player
		playerObject.Update(frameTime);

		playerObject.HandleSolidCollision(wallObject.GetHitBox());

		// -----------------------------------------------
		// Draw Section
		// -----------------------------------------------

		// Clear the window to a single colour
		gameWindow.clear(sf::Color::Black);

		// Draw everything to the window
		gameWindow.draw(titleText);

		// Draw the player
		playerObject.DrawTo(gameWindow);

		// Draw the wall
		wallObject.DrawTo(gameWindow);

		// Display the window contents on the screen 
		gameWindow.display();
	}
	return 0;
}