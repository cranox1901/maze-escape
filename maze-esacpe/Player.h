#pragma once
#include "AnimatingObject.h"

class Player : public AnimatingObject
{
public:

	// Constructors / Destructors
	Player(sf::Vector2f startingPos);

	// Functions to call Player-specific code
	void Input();
	void Update(sf::Time frameTime);
	void HandleSolidCollision(sf::FloatRect otherHitbox);

private:

	// Data
	sf::Vector2f velocity;
	sf::Vector2f previousPosition;
	float speed;
};
