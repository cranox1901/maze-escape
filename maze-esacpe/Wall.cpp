#include "Wall.h"

Wall::Wall(sf::Vector2f position)
	: SpriteObject(AssetManager::RequestTexture("Assets/Graphics/Wall.png"))
{
	sprite.setPosition(position);
}
